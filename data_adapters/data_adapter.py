from typing import *

import numpy as np
from sklearn.model_selection import KFold

from common import np_rng


class Adapter:
    def __init__(self, name: str):
        self.name = name
        self._one_hot_matrix = None
        self.x_train = None
        self.x_valid = None
        self.y_train = None
        self.y_valid = None

    @classmethod
    def get_batch_generator_from_generator(cls, data_generator: Generator,
                                           batch_size: int):
        batch_x = []
        batch_y = []
        for x, y in data_generator:
            batch_x.append(x)
            batch_y.append(y)
            if len(batch_x) == batch_size:
                yield np.asarray(batch_x), np.asarray(batch_y)
                batch_x = []
                batch_y = []
        return np.asarray(batch_x), np.asarray(batch_y)

    @classmethod
    def get_batch_generator_from_tensors(cls, x_data: np.ndarray, y_data: np.ndarray,
                                         batch_size: int):
        batch_x = []
        batch_y = []
        for i in range(len(x_data)):
            x, y = x_data[i], y_data[i]
            batch_x.append(x)
            batch_y.append(y)
            if len(batch_x) == batch_size:
                yield np.asarray(batch_x), np.asarray(batch_y)
                batch_x = []
                batch_y = []
        if len(batch_x) > 0:
            yield np.asarray(batch_x), np.asarray(batch_y)

    def get_train_generator(self):
        for x, y in zip(self.get_train_x(), self.get_train_y()):
            yield x, y

    def get_valid_generator(self):
        for x, y in zip(self.get_valid_x(), self.get_valid_y()):
            yield x, y

    def get_num_features(self):
        raise NotImplementedError

    def get_num_classes(self):
        raise NotImplementedError

    def get_train_x(self) -> np.ndarray:
        return self.x_train

    def get_train_y(self) -> np.ndarray:
        return self.y_train

    def get_valid_x(self) -> np.ndarray:
        return self.x_valid

    def get_valid_y(self) -> np.ndarray:
        return self.y_valid

    @classmethod
    def get_num_batches(cls, data: np.ndarray, batch_size: int) -> int:
        return len(data) // batch_size

    def train_num_batches(self, batch_size: int) -> int:
        return self.get_num_batches(self.get_train_x(), batch_size)

    def valid_num_batches(self, batch_size: int) -> int:
        return self.get_num_batches(self.get_valid_x(), batch_size)

    def cross_validate(self, kfolds: int, random_state: int = 0) -> Tuple[
        int, tuple, tuple]:
        """
        Generator of training cross validation. All result noting has to be handled by the user himself.
        Args:
            kfolds: Int. Number of folds to split.
            random_state: Int. Random state for random shuffling and splitting.
        Yields:
            Tuple of int and two tuples. First one contains `x_train` and `y_train`. Second one - `x_valid`
            and `y_valid`. Int is an index of fold.
        """
        x_data = np.concatenate((self.get_train_x(), self.get_valid_x()), axis=0)
        y_data = np.concatenate((self.get_train_y(), self.get_valid_y()), axis=0)
        kfolder = KFold(kfolds, shuffle=True, random_state=random_state)
        i = 0
        for train_index, valid_index in kfolder.split(x_data):
            x_train, y_train = x_data[train_index], y_data[train_index]
            x_valid, y_valid = x_data[valid_index], y_data[valid_index]
            yield i, (x_train, y_train), (x_valid, y_valid)
            i += 1

    def to_one_hot(self, data, nb_classes=None):
        if nb_classes is None:
            nb_classes = len(np.unique(data))
        if self._one_hot_matrix is None:
            hot_matrix = np.eye(nb_classes)
            self._one_hot_matrix = hot_matrix
        else:
            hot_matrix = self._one_hot_matrix
        return hot_matrix[data]

    def get_input_shape(self):
        raise NotImplementedError


def get_random_choice_mask_for_dataset(length: int, fraction: float) -> np.ndarray:
    mask = np.zeros((length,), dtype=np.bool)
    indices_to_mask = np_rng.choice(np.arange(0, length), size=int(fraction * length))
    mask[indices_to_mask] = True
    return mask
