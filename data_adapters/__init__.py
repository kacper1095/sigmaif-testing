from .breast_cancer import BreastCancerAdapter
from .car import CarAdapter
from .cifar import Cifar100Adapter, Cifar10Adapter
from .credit_approval import CreditApprovalAdapter
from .data_adapter import Adapter
from .iris import IrisAdapter
from .mnist import MnistAdapter
from .sonar import SonarAdapter
from .urban_land_cover import UrbanLandCover
from .votes import VotesAdapter
from .wine import WineAdapter
