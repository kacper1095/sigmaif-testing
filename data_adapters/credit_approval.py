import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder

from common import DATASETS_PATH
from data_adapters.data_adapter import Adapter


class CreditApprovalAdapter(Adapter):
    def __init__(self):
        super().__init__("crx")
        self._column_names = list(range(15)) + ["class"]

        categorical_columns = [0, 3, 4, 5, 6, 8, 9, 11, 12]
        continuous_columns = [1, 2, 7, 10, 13, 14]

        self._features_column = self._column_names[:-1]
        self._class_column = self._column_names[-1]
        self._raw_data = pd.read_csv(DATASETS_PATH["credit-approval"] / "crx.data",
                                     header=None,
                                     names=self._column_names)
        self._raw_data[self._raw_data == "?"] = np.nan
        self._raw_data.dropna(inplace=True)

        self._x = np.concatenate([
            self._raw_data[continuous_columns].values,
            pd.get_dummies(self._raw_data[categorical_columns]).values
        ], axis=1)

        self._x = self._x.astype(np.float32)
        self._y = self._raw_data[self._class_column].values

        self._le = LabelEncoder()
        self._le.fit(self._y)
        self._y = self._le.transform(self._y)
        self._y = self.to_one_hot(self._y)

        self._rng = np.random.RandomState(0)
        permutation = self._rng.permutation(range(len(self._x)))
        self._x, self._y = self._x[permutation], self._y[permutation]

        split_index = int(0.8 * len(self._x))

        self.x_train, self.y_train = self._x[:split_index], self._y[:split_index]
        self.x_valid, self.y_valid = self._x[split_index:], self._y[split_index:]

    def get_num_features(self):
        return len(self._x[0])

    def get_num_classes(self):
        return len(self._le.classes_)

    def get_input_shape(self):
        return None, self.get_num_features()
