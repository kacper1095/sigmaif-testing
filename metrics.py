import tensorflow as tf

from common import EPSILON

__all__ = ["get_accuracy", "get_fscore"]


def get_accuracy(true_one_hot: tf.Tensor, predicted_one_hot: tf.Tensor) -> tf.Tensor:
    return tf.reduce_mean(tf.cast(tf.equal(
        tf.argmax(true_one_hot, axis=-1),
        tf.argmax(predicted_one_hot, axis=-1)
    ), dtype=tf.float32))


def get_fscore(true_one_hot: tf.Tensor, predicted_one_hot: tf.Tensor) -> tf.Tensor:
    def recall(true_one_hot: tf.Tensor, predicted_one_hot: tf.Tensor) -> tf.Tensor:
        true_positives = tf.reduce_sum(tf.round(tf.clip_by_value(
            true_one_hot * predicted_one_hot, 0, 1
        )))
        possible_positives = tf.reduce_sum(tf.round(tf.clip_by_value(
            true_one_hot, 0, 1
        )))
        recall = true_positives / (possible_positives + EPSILON)
        return recall

    def precision(true_one_hot: tf.Tensor, predicted_one_hot: tf.Tensor) -> tf.Tensor:
        true_positives = tf.reduce_sum(tf.round(tf.clip_by_value(
            true_one_hot * predicted_one_hot, 0, 1
        )))
        predicted_positives = tf.reduce_sum(tf.round(tf.clip_by_value(
            predicted_one_hot, 0, 1
        )))
        precision = true_positives / (predicted_positives + EPSILON)
        return precision

    prec = precision(true_one_hot, predicted_one_hot)
    rec = recall(true_one_hot, predicted_one_hot)
    return 2 * ((prec * rec) / (prec + rec + EPSILON))
