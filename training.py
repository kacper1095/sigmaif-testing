"""
This file was created because for small datasets Estimator is inefficient because of constant model reloading
which causes prolonging learning that in fact should be short.
"""

import tensorflow as tf

from data_adapters import *
from data_augmentators import *
from metrics import *
from model import Model
from models_classes import *
from normalizers import *

tf.logging.set_verbosity(tf.logging.INFO)
tf.set_random_seed(0)


def mean_squared_error(labels, logits):
    return tf.reduce_mean(
        tf.reduce_sum(tf.square(tf.nn.sigmoid(logits) - labels), axis=-1))


def main(_):
    adapter = MnistAdapter()
    # adapter = SonarAdapter()
    # adapter = Cifar10Adapter()
    model_fn = SigmaIfFn([adapter.get_num_classes()], number_of_groups=784)

    model = Model(
        adapter.get_input_shape(), adapter.get_num_classes(),
        # model_fn=DenseWithBatchNormFn([128, adapter.get_num_classes()])
        # model_fn=DenseFn([128, adapter.get_num_classes()])
        # model_fn=SigmaIfFn([128, adapter.get_num_classes()], threshold=0.0)
        # model_fn=ProductUnitFn([128, adapter.get_num_classes()])
        # model_fn=ClusteronFn([16, adapter.get_num_classes()], 3)
        # model_fn=FFASigmaIfFn([1024, adapter.get_num_classes()], threshold=0.2, number_of_groups=1)
        # model_fn=SigmaIfBatchNormFn([1024, adapter.get_num_classes()], threshold=0.2)
        # model_fn=FFASigmaIfBatchNormFn([100, adapter.get_num_classes()], forward_fading=0.97, backward_fading=0.97,
        #                                threshold=0.1)
        model_fn=model_fn
    )

    model.compile(
        tf.nn.softmax_cross_entropy_with_logits_v2,
        # mean_squared_error,
        # tf.train.AdamOptimizer(0.001),
        tf.train.MomentumOptimizer(0.01, 0.8),
        metrics={"acc": get_accuracy, "fscore": get_fscore}
    )

    augmentator = AdditiveGaussianNoise(0, 1, 0xCAFFE)

    # normalizer = ZeroToOneNormalizer(axis=0)
    # normalizer = ZeroToOneNormalizer(min_value=0, max_value=255)
    # normalizer = MinusOneToOneNormalizer(axis=0)
    normalizer = MinusOneToOneNormalizer(min_value=0, max_value=255)
    # normalizer = StandardNormalizer()
    # normalizer = NoopNormalizer()

    for index, train, valid in adapter.cross_validate(args.kfolds, 0xCAFFE):
        tf.logging.info("Fold: {}/{}".format(index + 1, args.kfolds))
        x_train, y_train = train
        x_valid, y_valid = valid
        x_train, x_valid = normalizer.normalize(x_train, x_valid)
        # model_path = common.get_model_path(os.path.join(args.model_output, str(index)))
        train_history, valid_history = model.train(
            lambda: adapter.get_batch_generator_from_tensors(x_train, y_train,
                                                             args.batch_size),
            # lambda: augmentator.augment_from_generator(
            #     adapter.get_batch_generator_from_tensors(x_train, y_train, args.batch_size)),
            lambda: adapter.get_batch_generator_from_tensors(x_valid, y_valid,
                                                             args.batch_size),
            # lambda: augmentator.augment_from_generator(
            #     adapter.get_batch_generator_from_tensors(x_valid, y_valid, args.batch_size)),
            # adapter.get_num_batches(x_train, args.batch_size), args.epochs, 100, model_path.as_posix(),
            # model_path.as_posix(), True, 100
            adapter.get_num_batches(x_train, args.batch_size), args.epochs, 1, None,
            None, True, 100
        )
        break
        # pd.DataFrame(train_history).to_csv(model_path / "train_history_{}.csv".format(index), index=False)
        # pd.DataFrame(valid_history).to_csv(model_path / "valid_history_{}.csv".format(index), index=False)


if __name__ == '__main__':
    from flags import get_flags

    args = get_flags()
    tf.app.run()
