import gzip
import pickle
from urllib import request

import numpy as np

from common import DATASETS_PATH, IMAGE_DATASET_USED_FRACTION
from data_adapters.data_adapter import Adapter, get_random_choice_mask_for_dataset

filename = [
    ["training_images", "train-images-idx3-ubyte.gz"],
    ["test_images", "t10k-images-idx3-ubyte.gz"],
    ["training_labels", "train-labels-idx1-ubyte.gz"],
    ["test_labels", "t10k-labels-idx1-ubyte.gz"]
]


def download_mnist():
    base_url = "http://yann.lecun.com/exdb/mnist/"
    for name in filename:
        print("Downloading " + name[1] + "...")
        request.urlretrieve(base_url + name[1], name[1])
    print("Download complete.")


def save_mnist():
    mnist = {}
    for name in filename[:2]:
        with gzip.open(name[1], 'rb') as f:
            mnist[name[0]] = np.frombuffer(f.read(), np.uint8, offset=16).reshape(-1,
                                                                                  28 * 28)
    for name in filename[-2:]:
        with gzip.open(name[1], 'rb') as f:
            mnist[name[0]] = np.frombuffer(f.read(), np.uint8, offset=8)
    with open(DATASETS_PATH['mnist'] / "mnist.pkl", 'wb') as f:
        pickle.dump(mnist, f)
    print("Save complete.")


def init():
    download_mnist()
    save_mnist()


def load():
    with open(DATASETS_PATH['mnist'] / "mnist.pkl", 'rb') as f:
        mnist = pickle.load(f)
    return mnist["training_images"], mnist["training_labels"], mnist["test_images"], mnist[
        "test_labels"]


class MnistAdapter(Adapter):
    def __init__(self):
        super().__init__("mnist")
        dataset = load()
        self.x_train, self.y_train = dataset[0], dataset[1]
        self.x_valid, self.y_valid = dataset[2], dataset[3]

        self.x_train = np.reshape(self.x_train, (-1, 28, 28, 1)).astype(np.float32)
        self.x_valid = np.reshape(self.x_valid, (-1, 28, 28, 1)).astype(np.float32)

        train_mask = get_random_choice_mask_for_dataset(self.x_train.shape[0],
                                                        IMAGE_DATASET_USED_FRACTION)
        valid_mask = get_random_choice_mask_for_dataset(self.x_valid.shape[0],
                                                        IMAGE_DATASET_USED_FRACTION)

        self.x_train = self.x_train.astype(np.float32)
        self.x_train = self.x_train[train_mask]

        self.y_train = self.to_one_hot(self.y_train)
        self.y_train = self.y_train[train_mask]

        self.x_valid = self.x_valid.astype(np.float32)
        self.x_valid = self.x_valid[valid_mask]

        self.y_valid = self.to_one_hot(self.y_valid)
        self.y_valid = self.y_valid[valid_mask]

    def get_num_features(self):
        return 28 * 28 * 1

    def get_num_classes(self):
        return 10

    def get_input_shape(self):
        return None, 28, 28, 1


if __name__ == '__main__':
    init()
