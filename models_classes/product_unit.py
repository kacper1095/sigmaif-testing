import math
from typing import *

import tensorflow as tf

from model_fn import ModelFn


def product_unit_layer(inputs: tf.Tensor, units: int, use_bias: bool,
                       layer_name: str) -> tf.Tensor:
    input_shape = inputs.get_shape().as_list()[-1]
    x = inputs
    limit = 1e-2
    with tf.variable_scope(layer_name):
        offset = tf.Variable(tf.zeros((1, input_shape)), name="offset")
        x = x - offset
        weights = tf.Variable(
            tf.random_uniform((input_shape, units), -limit, limit, dtype=tf.float32),
            dtype=tf.float32, name="W")
        mask = tf.cast(x < 0, dtype=tf.float32)
        exponent = tf.exp(tf.matmul(tf.log(tf.abs(x) + 1e-5), weights))
        real_part = tf.cos(math.pi * tf.matmul(mask, weights))
        x = exponent * real_part
        if use_bias:
            biases = tf.Variable(-tf.ones((1, units)), dtype=tf.float32, name="b")
            x = x + biases
    return x


class ProductUnitFn(ModelFn):
    def __init__(self, layer_sizes: List[int]):
        self.layer_sizes = layer_sizes

    def call(self, input_data, resort, is_training, classes):
        input_data = tf.layers.flatten(input_data)
        net = input_data
        neural_activities = {}
        for i, size in enumerate(self.layer_sizes):
            net = product_unit_layer(net, size, False, "layer_{}".format(i))
            net = tf.layers.dense(net, size)
            activity = tf.constant(1, dtype=tf.float32)
            tf.summary.scalar(f"activity_{i}", activity)
            neural_activities[f"activity_{i}"] = activity
            if i < len(self.layer_sizes) - 1:
                net = tf.nn.sigmoid(net)
        logits = net
        y_pred = tf.nn.softmax(logits)

        y_pred_cls = tf.argmax(y_pred, axis=1)
        return logits, y_pred, y_pred_cls, neural_activities
