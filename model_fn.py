class ModelFn(object):
    def call(self, input_data, resort, is_training, classes):
        """
        Helper function to subclass from `ModelFn` which helps to construct trained_models for script `train_without_estimator`.
        Args:
            input_data: tf.placeholder of data to input. Necessary field.
            resort: tf.placeholder of boolean flag for sorting. Necessary for sigmaif trained_models, can be None otherwise.
            classes: Int, number of classes.
        Returns:
            Built graph using functional API with tensorflow.
        """
        raise NotImplementedError

    def __call__(self, input_data, resort, is_training, classes):
        return self.call(input_data, resort, is_training, classes)
