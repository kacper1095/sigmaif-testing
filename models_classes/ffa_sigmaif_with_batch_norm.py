from typing import *

import tensorflow as tf
from sigmaif.group_classes_initializers import *
from sigmaif.layer import ffa_sigmaif

from model_fn import ModelFn
from utils.neural_activity_holder import AllNeuralActivityHolder


class FFASigmaIfBatchNormFn(ModelFn):
    def __init__(self, layer_sizes: List[int], forward_fading: float = 0.7,
                 backward_fading: float = 1.0,
                 threshold: float = 0.6, number_of_groups: int = 7):
        self.threshold = threshold
        self.layer_sizes = layer_sizes
        self.forward_fading = forward_fading
        self.backward_fading = backward_fading
        self.number_of_groups = number_of_groups

    def call(self, input_data, resort, is_training, classes):
        input_data = tf.layers.flatten(input_data)
        net = input_data
        input_features = net.get_shape().as_list()[-1]
        all_activites = AllNeuralActivityHolder()
        for i, size in enumerate(self.layer_sizes):
            net, neuron_activity = ffa_sigmaif([net, resort], size, self.threshold,
                                               forward_fading_factor=self.forward_fading,
                                               backward_fading_factor=self.backward_fading,
                                               kernel_initializer=tf.keras.initializers.he_uniform(),
                                               group_initializer=Regular(
                                                   (
                                                           input_features + self.number_of_groups - 1) // self.number_of_groups))
            out_features = net.get_shape().as_list()[-1]

            all_activites.add_data(neuron_activity)
            all_activites.add_shape((input_features, out_features))

            if i < len(self.layer_sizes) - 1:
                net = tf.layers.batch_normalization(net, training=is_training, renorm=True)
                net = tf.nn.sigmoid(net)
            input_features = out_features
        tf.summary.scalar("activity", all_activites.get_percentage_activity())
        logits = net
        y_pred = tf.nn.softmax(logits)

        y_pred_cls = tf.argmax(y_pred, axis=1)
        return logits, y_pred, y_pred_cls, {
            "activity": all_activites.get_percentage_activity()}
