import datetime
import random
from pathlib import Path

import numpy as np
import tensorflow as tf

DATA_PATH = Path("datasets")

DATASETS_PATH = {
    "sonar": DATA_PATH / "sonar",
    "mnist": DATA_PATH / "mnist",
    "cifar10": DATA_PATH / "cifar10",
    "cifar100": DATA_PATH / "cifar100",
    "breast-cancer": DATA_PATH / "breast-cancer",
    "car": DATA_PATH / "car",
    "credit-approval": DATA_PATH / "credit-approval",
    "iris": DATA_PATH / "iris",
    "urban-land-cover": DATA_PATH / "urban-land-cover",
    "votes": DATA_PATH / "votes",
    "wine": DATA_PATH / "wine"
}

PROGRESSBAR_STATS_MOMENTUM = 0.8
SUMMARY_COLLECTION = "summary_collection"
EPSILON = 1e-8
EXPERIMENTS_FOLDER = "experiments/"
IMAGE_DATASET_USED_FRACTION = 0.1

np_rng = np.random.RandomState(0xCAFFE)
py_rng = random.Random(0xCAFFE)

DROPOUT_RATE = 0.4


def get_model_path(name: str) -> Path:
    a_path = Path(name)
    a_path.mkdir(parents=True)
    return a_path


def gaussian_dropout(inputs: tf.Tensor, is_training: tf.Tensor) -> tf.Tensor:
    return tf.cond(
        is_training,
        true_fn=lambda: inputs * tf.random_normal(shape=tf.shape(inputs),
                                                  mean=1,
                                                  stddev=np.sqrt(
                                                      DROPOUT_RATE / (1 - DROPOUT_RATE))),
        false_fn=lambda: inputs
    )


def get_group_count(input_size: int, group_size: int) -> int:
    return (input_size + group_size - 1) // group_size


def get_timestamp() -> str:
    return datetime.datetime.now().strftime("%HH_%MM_%dd_%mm_%Yy")
