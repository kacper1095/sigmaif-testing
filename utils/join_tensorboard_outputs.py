import argparse

import pandas as pd


def join(paths, names):
    assert len(paths) == len(names), "Names and paths have to have the same length."
    output = None
    for path, name in zip(args.csvs, args.names):
        if output is None:
            output = pd.read_csv(path)
            output.rename({"Value": name}, axis="columns", inplace=True)
            output.set_index("Step", inplace=True)
        else:
            other = pd.read_csv(path)
            other.rename({"Value": name}, axis="columns", inplace=True)
            other["Step"] -= 1
            other.set_index("Step", inplace=True)
            other.drop("Wall time", axis=1, inplace=True)
            output = output.join(other, how="left")
    return output


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        "Script for joining csv outputs downloaded from tensorboard utility. Joins by step.")
    parser.add_argument("-c", "--csvs", nargs="+",
                        help="Variable length list of csvs paths to join.")
    parser.add_argument("-n", "--names", nargs="+",
                        help="Names of columns to use instead of default 'Value' in the csv. "
                             "There has to be the same number of names as there are csvs.")
    parser.add_argument("-o", "--output", help="Output path for single combined csv.")

    args = parser.parse_args()
    out_file = join(args.csvs, args.names)
    out_file.to_csv(args.output)
