from pprint import pprint

import numpy as np

from data_adapters import *
from experiments import KFOLDS

datasets = [
    lambda: BreastCancerAdapter(),
    lambda: Cifar10Adapter(),
    lambda: CreditApprovalAdapter(),
    lambda: MnistAdapter(),
    lambda: SonarAdapter(),
    lambda: UrbanLandCover(),
    lambda: WineAdapter(),
]

out = []

for dataset in datasets:
    dataset_instance = dataset()
    out.append({
        "name": dataset_instance.name,
        "train_len": [],
        "valid_len": []
    })

    for i, train_fold, valid_fold in dataset_instance.cross_validate(KFOLDS):
        x_train, y_train = train_fold
        x_valid, y_valid = valid_fold

        out[-1]["train_len"].append(len(x_train))
        out[-1]["valid_len"].append(len(x_valid))

    out[-1]["train_len"] = np.round((np.mean(out[-1]["train_len"])).item())
    out[-1]["valid_len"] = np.round((np.mean(out[-1]["valid_len"])).item())

pprint(out)
