import os
import pprint
import time
from collections import OrderedDict, defaultdict
from typing import *

import numpy as np
import tensorflow as tf
import tqdm
from tensorboardX.writer import SummaryWriter


class PatienceModel:
    def __init__(self):
        self.current_patience: int = 0
        self.should_proceed: bool = True


class Model:
    def __init__(self, input_shape: tuple, num_output_classes: int,
                 model_fn: Callable):
        self._x = tf.placeholder(tf.float32, input_shape, name="input_tensor")
        self._y = tf.placeholder(tf.float32, (None, num_output_classes),
                                 name="output_tensor")
        self._resort = tf.placeholder(tf.bool, name="resort_tensor")
        self._is_training = tf.placeholder(tf.bool, name="is_training_flag")
        self._logits, self._output, self._predicted_class, self._neural_activities = model_fn(
            self._x, self._resort,
            self._is_training,
            num_output_classes)

        self._loss: Optional[tf.Operation] = None
        self._optimiser: Optional[tf.train.Optimizer] = None
        self._session: Optional[tf.Session] = None

        self._metrics = {}

        self.global_step = 0

    def compile(self, logit_loss_fn: Callable, optimiser: tf.train.Optimizer,
                metrics: Dict[str, Callable]):
        self._loss = tf.reduce_mean(logit_loss_fn(labels=self._y, logits=self._logits))
        extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(extra_update_ops):
            self._optimiser = optimiser.minimize(self._loss)

        self._metrics = {
            key: fun(true_one_hot=self._y, predicted_one_hot=self._output)
            for key, fun in metrics.items()
        }
        for key, metric in self._metrics.items():
            tf.summary.scalar(key, metric)
        tf.summary.scalar("loss", self._loss)
        self._metrics.update(self._neural_activities)

        self._session = tf.Session()
        self._session.run(tf.global_variables_initializer())

    def load_model(self, model_path: str):
        saver = tf.train.Saver()
        ckpt_state = tf.train.get_checkpoint_state(model_path)
        model_path = os.path.join(model_path,
                                  os.path.basename(ckpt_state.model_checkpoint_path))
        saver.restore(self._session, model_path)

    def train(self, train_generator_fn: Callable, valid_generator_fn: Callable,
              nb_steps: int, nb_epochs: int, resort_step: int, logdir: Optional[str],
              model_save_path: Optional[str],
              verbose: bool, early_stopping_patience: int):
        if verbose:
            tf.logging.info("Number of parameters: {}".format(self.num_parameters))

        train_writer = tf.summary.FileWriter(os.path.join(logdir, "train"),
                                             self._session.graph) if logdir else None
        valid_writer = SummaryWriter(os.path.join(logdir, "valid")) if logdir else None
        saver = tf.train.Saver() if model_save_path else None
        best_loss = np.inf
        self.global_step = 0

        train_learning_history = defaultdict(list)
        valid_learning_history = defaultdict(list)

        merged = tf.summary.merge_all()
        current_patience = 0
        for epoch in range(nb_epochs):
            tf.logging.info("Epoch {}/{}".format(epoch + 1, nb_epochs))
            train_stats = defaultdict(list)
            valid_stats = defaultdict(list)

            # Training steps
            with tqdm.tqdm(total=nb_steps) as pbar:
                for batch_x, batch_y in train_generator_fn():
                    start_time = time.time()
                    resort_cond = (self.global_step + 1) % resort_step == 0
                    summary, loss, _, metrics = self._session.run(
                        [merged, self._loss, self._optimiser, self._metrics],
                        feed_dict={
                            self._x: batch_x,
                            self._y: batch_y,
                            self._resort: resort_cond,
                            self._is_training: True
                        }
                    )
                    step_time = time.time() - start_time

                    train_learning_history["global_step"].append(self.global_step)
                    train_learning_history["step_time"].append(step_time)
                    train_learning_history["loss"].append(loss)
                    train_stats["loss"].append(loss)
                    train_stats["epoch"].append(epoch)
                    for key, value in metrics.items():
                        train_learning_history[key].append(value)
                        train_stats[key].append(value)

                    train_writer is not None and train_writer.add_summary(summary,
                                                                          self.global_step)

                    self.global_step += 1
                    pbar.update(1)
                    pbar.set_postfix(OrderedDict([
                        (key, "%.4f" % np.mean(values).item())
                        for key, values in train_stats.items()
                    ]))

            # Validation steps
            for batch_x, batch_y in valid_generator_fn():
                start_time = time.time()
                loss, metrics = self._session.run([self._loss, self._metrics],
                                                  feed_dict={
                                                      self._x: batch_x,
                                                      self._y: batch_y,
                                                      self._resort: False,
                                                      self._is_training: False
                                                  })
                step_time = time.time() - start_time
                valid_stats["loss"].append(loss)
                valid_stats["step_time"].append(step_time)
                for key, value in metrics.items():
                    valid_stats[key].append(value)

            valid_learning_history["global_step"].append(self.global_step)
            valid_learning_history["epoch"].append(epoch)
            averaged_valid_results = {}
            for key, values in valid_stats.items():
                value = np.mean(values).item()
                valid_learning_history[key].append(value)
                valid_writer and valid_writer.add_scalar(key, value, self.global_step)
                averaged_valid_results[key] = value
            if verbose:
                tf.logging.info("\n" + pprint.pformat(averaged_valid_results, indent=2))

            if averaged_valid_results["loss"] < best_loss:
                if verbose:
                    tf.logging.info("Improved loss from %.4f to %.4f" % (
                        best_loss, averaged_valid_results["loss"]))
                best_loss = averaged_valid_results["loss"]
                if saver is not None:
                    saver.save(self._session, os.path.join(model_save_path, "model.ckpt"))
                current_patience = 0
            else:
                current_patience += 1
            if current_patience > early_stopping_patience:
                break

        return train_learning_history, valid_learning_history

    def train_with_multiple_valid_datasets(
            self,
            train_generator_fn: Callable,
            valid_generator_fns: Dict[str, Callable],
            nb_steps: int,
            nb_epochs: int,
            resort_step: int,
            verbose: bool,
            early_stopping_patience: int):
        """
        Method for a master thesis purpose, do not use it normal conditions since it's unsafe.
        Args:
            train_generator_fn: Callable generating batches of training data.
            valid_generator_fns: Dict in a format {name_of_generator -> Callable as for training data}.
            nb_steps: Number of training steps to perform for each data.
            nb_epochs: Number of training epochs to perform.
            resort_step: Number of steps after neural connections should be sorted.
            verbose: Bool whether training should be verbose.
            early_stopping_patience: Patience separate for each of valid datasets.
        Returns:
            Tuple containing training dataset history, as if it was normal training, and training history
            for each of validating generators.
        """
        if verbose:
            tf.logging.info("Number of parameters: {}".format(self.num_parameters))

        best_losses = {
            name: np.inf for name in valid_generator_fns.keys()
        }

        self.global_step = 0

        train_learning_history = defaultdict(list)
        valid_learning_histories = {
            name: defaultdict(list) for name in valid_generator_fns.keys()
        }

        merged = tf.summary.merge_all()
        patiences: Dict[str, PatienceModel] = {
            name: PatienceModel() for name in valid_generator_fns.keys()
        }

        for epoch in range(nb_epochs):
            tf.logging.info("Epoch {}/{}".format(epoch + 1, nb_epochs))
            train_stats = defaultdict(list)

            # Training steps
            with tqdm.tqdm(total=nb_steps) as pbar:
                for batch_x, batch_y in train_generator_fn():
                    start_time = time.time()
                    summary, loss, _, metrics = self._session.run([merged, self._loss,
                                                                   self._optimiser,
                                                                   self._metrics],
                                                                  feed_dict={
                                                                      self._x: batch_x,
                                                                      self._y: batch_y,
                                                                      self._resort: (
                                                                                            self.global_step + 1) % resort_step == 0,
                                                                      self._is_training: True
                                                                  })
                    step_time = time.time() - start_time

                    train_learning_history["global_step"].append(self.global_step)
                    train_learning_history["step_time"].append(step_time)
                    train_learning_history["loss"].append(loss)
                    train_stats["loss"].append(loss)
                    train_stats["epoch"].append(epoch)
                    for key, value in metrics.items():
                        train_learning_history[key].append(value)
                        train_stats[key].append(value)

                    self.global_step += 1
                    pbar.update(1)
                    pbar.set_postfix(OrderedDict([
                        (key, "%.4f" % np.mean(values).item())
                        for key, values in train_stats.items()
                    ]))

            # Validation steps
            for validation_name, valid_generator_fn in valid_generator_fns.items():
                valid_stats = defaultdict(list)
                if not patiences[validation_name].should_proceed:
                    continue
                for batch_x, batch_y in valid_generator_fn():
                    start_time = time.time()
                    loss, metrics = self._session.run([self._loss, self._metrics],
                                                      feed_dict={
                                                          self._x: batch_x,
                                                          self._y: batch_y,
                                                          self._resort: False,
                                                          self._is_training: False
                                                      })
                    step_time = time.time() - start_time
                    valid_stats["loss"].append(loss)
                    valid_stats["step_time"].append(step_time)
                    for key, value in metrics.items():
                        valid_stats[key].append(value)

                valid_learning_histories[validation_name]["global_step"].append(
                    self.global_step)
                valid_learning_histories[validation_name]["epoch"].append(epoch)

                averaged_valid_results = {}
                for key, values in valid_stats.items():
                    value = np.mean(values).item()

                    valid_learning_histories[validation_name][key].append(value)
                    averaged_valid_results[key] = value
                if verbose:
                    tf.logging.info(
                        "\n" + pprint.pformat(averaged_valid_results, indent=2))

                if averaged_valid_results["loss"] < best_losses[validation_name]:
                    if verbose:
                        tf.logging.info("Improved loss from %.4f to %.4f" % (
                            best_losses[validation_name], averaged_valid_results["loss"]))
                    best_losses[validation_name] = averaged_valid_results["loss"]
                    patiences[validation_name].current_patience = 0
                else:
                    patiences[validation_name].current_patience += 1
                if patiences[validation_name].current_patience > early_stopping_patience:
                    patiences[validation_name].should_proceed = False

            if all(not patience.should_proceed for name, patience in patiences.items()):
                break

        return train_learning_history, valid_learning_histories

    def score(self, data_generator_fn: Callable, verbose: bool):
        """
        Method for scoring model according to given data generator.
        Args:
            data_generator_fn: Callable generating batches of data to evaluate on.
            verbose: Bool whether scoring should be verbose.
        Returns:
            Dict containing name of metrics and value of metric.
        """
        if verbose:
            tf.logging.info("Number of parameters: {}".format(self.num_parameters))

        # Validation steps
        valid_stats = defaultdict(list)
        for batch_x, batch_y in data_generator_fn():
            start_time = time.time()
            loss, metrics = self._session.run([self._loss, self._metrics],
                                              feed_dict={
                                                  self._x: batch_x,
                                                  self._y: batch_y,
                                                  self._resort: False,
                                                  self._is_training: False
                                              })
            step_time = time.time() - start_time
            valid_stats["loss"].append(loss)
            valid_stats["step_time"].append(step_time)
            for key, value in metrics.items():
                valid_stats[key].append(value)

        averaged_valid_results = {}
        for key, values in valid_stats.items():
            value = np.mean(values).item()

            averaged_valid_results[key] = value
        if verbose:
            tf.logging.info("\n" + pprint.pformat(averaged_valid_results, indent=2))

        return averaged_valid_results

    @property
    def num_parameters(self):
        num_params = 0
        for var in tf.trainable_variables():
            num_params += np.prod(var.get_shape().as_list())
        return num_params
