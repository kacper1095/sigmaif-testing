import abc
import json
import os
from typing import Callable

import pandas as pd
import tensorflow as tf

import common
from data_adapters import Adapter
from metrics import get_accuracy, get_fscore
from model import Model
from normalizers import Normalizer


class _Description(abc.ABC):
    def __init__(self, name: str,
                 model_fn: Callable,
                 dataset: Adapter,
                 data_normalizer: Normalizer,
                 kfolds: int,
                 repetitions: int,
                 batch_size: int,
                 epochs: int,
                 patience: int,
                 loss_fn: Callable,
                 optimiser: tf.train.Optimizer):
        self.name = name
        self.model_fn = model_fn
        self.dataset = dataset
        self.data_normalizer = data_normalizer
        self.batch_size = batch_size
        self.epochs = epochs
        self.kfolds = kfolds
        self.loss_fn = loss_fn
        self.optimiser = optimiser
        self.repetitions = repetitions
        self.patience = patience

    def run(self, **kwargs):
        tf.logging.set_verbosity(tf.logging.INFO)
        for repetition in range(self.repetitions):
            tf.logging.info("Repetition: {}/{}".format(repetition + 1, self.repetitions))
            for index, train, valid in self.dataset.cross_validate(self.kfolds):
                tf.logging.info("Fold: {}/{}".format(index + 1, self.kfolds))
                with tf.Graph().as_default():
                    model = Model(self.dataset.get_input_shape(),
                                  self.dataset.get_num_classes(),
                                  model_fn=self.model_fn)

                    model.compile(
                        self.loss_fn,
                        self.optimiser,
                        metrics={"acc": get_accuracy, "fscore": get_fscore}
                    )

                    x_train, y_train = train
                    x_valid, y_valid = valid
                    x_train, x_valid = self.data_normalizer.normalize(x_train, x_valid)
                    train_history, valid_history = self._run_single_loop(
                        model,
                        (x_train, y_train),
                        (x_valid, y_valid),
                        self.name, repetition, index,
                        **kwargs
                    )

                    self._save_loop_results(train_history, valid_history, self.name,
                                            repetition, index)

    @classmethod
    def get_path(cls, name, repetition, cv_index):
        experiment_name = os.path.join(EXPERIMENT_FOLDER, name)
        model_path = common.get_model_path(os.path.join(experiment_name,
                                                        "rep_%d" % repetition,
                                                        "cv_%d" % cv_index))
        return model_path

    @abc.abstractmethod
    def _run_single_loop(self, model, train, valid, name, repetition, cv_index, **kwargs):
        pass

    @abc.abstractmethod
    def _save_loop_results(self, train_history, valid_history, name, repetition, cv_index):
        pass


class StandardDescription(_Description):
    def _run_single_loop(self, model, train, valid, name, repetition, cv_index, **kwargs):
        x_train, y_train = train
        x_valid, y_valid = valid

        train_history, valid_history = model.train(
            lambda: self.dataset.get_batch_generator_from_tensors(x_train,
                                                                  y_train,
                                                                  self.batch_size),
            lambda: self.dataset.get_batch_generator_from_tensors(x_valid, y_valid,
                                                                  self.batch_size),
            self.dataset.get_num_batches(x_train, self.batch_size),
            self.epochs, 100, None,
            None, False,
            self.patience
        )

        return train_history, valid_history

    def _save_loop_results(self, train_history, valid_history, name, repetition, cv_index):
        model_path = self.get_path(name, repetition, cv_index)
        pd.DataFrame(train_history).to_csv(model_path / "train_history.csv",
                                           index=False)
        pd.DataFrame(valid_history).to_csv(model_path / "valid_history.csv",
                                           index=False)


class CustomScoringAugmentatorsDescription(_Description):
    def _run_single_loop(self, model, train, valid, name, repetition, cv_index, **kwargs):
        augmentators = kwargs.get("augmentators")
        x_train, y_train = train
        x_valid, y_valid = valid
        train_history, valid_history = model.train(
            lambda: self.dataset.get_batch_generator_from_tensors(x_train,
                                                                  y_train,
                                                                  self.batch_size),
            lambda: self.dataset.get_batch_generator_from_tensors(x_valid, y_valid,
                                                                  self.batch_size),
            self.dataset.get_num_batches(x_train, self.batch_size),
            self.epochs, 100, None,
            None, False,
            self.patience
        )

        for name, augmentator in augmentators.items():
            score = model.score(lambda: augmentator.augment_from_generator(
                self.dataset.get_batch_generator_from_tensors(x_valid, y_valid,
                                                              self.batch_size)
            ), False)
            experiment_name = os.path.join(EXPERIMENT_FOLDER, self.name)
            model_path = common.get_model_path(os.path.join(experiment_name,
                                                            name,
                                                            "rep_%d" % repetition,
                                                            "cv_%d" % cv_index))
            pd.DataFrame(train_history).to_csv(
                model_path / "train_history.csv", index=False)
            pd.DataFrame(valid_history).to_csv(
                model_path / "valid_history.csv", index=False)
            (model_path / "valid_score.json").write_text(
                json.dumps(score, indent=4))

        return None, None

    def _save_loop_results(self, train_history, valid_history, name, repetition, cv_index):
        pass


class LearnAsMlpPredictAsCxNnDescription(_Description):
    def _run_single_loop(self, model, train, valid, name, repetition, cv_index, **kwargs):
        group_space = kwargs.get("groups")
        threshold_space = kwargs.get("threshold")
        x_train, y_train = train
        x_valid, y_valid = valid
        train_history, valid_history = model.train(
            lambda: self.dataset.get_batch_generator_from_tensors(x_train,
                                                                  y_train,
                                                                  self.batch_size),
            lambda: self.dataset.get_batch_generator_from_tensors(x_valid, y_valid,
                                                                  self.batch_size),
            self.dataset.get_num_batches(x_train, self.batch_size),
            self.epochs, 100, None,
            None, False,
            self.patience
        )

        for nb_groups in group_space:
            for threshold in threshold_space:
                score = model.score(
                    lambda: self.dataset.get_batch_generator_from_tensors(x_valid, y_valid,
                                                                          self.batch_size),
                    False)
                experiment_name = os.path.join(EXPERIMENT_FOLDER, self.name)
                model_path = common.get_model_path(os.path.join(experiment_name,
                                                                f"{nb_groups}_{threshold}",
                                                                "rep_%d" % repetition,
                                                                "cv_%d" % cv_index))
                pd.DataFrame(train_history).to_csv(
                    model_path / "train_history.csv", index=False)
                pd.DataFrame(valid_history).to_csv(
                    model_path / "valid_history.csv", index=False)
                (model_path / "valid_score.json").write_text(
                    json.dumps(score, indent=4))

        return None, None

    def _save_loop_results(self, train_history, valid_history, name, repetition, cv_index):
        pass


run_time_stamp = common.get_timestamp()
EXPERIMENT_FOLDER = os.path.join(common.EXPERIMENTS_FOLDER, run_time_stamp)
