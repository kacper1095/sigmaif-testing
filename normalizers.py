from typing import *

import numpy as np


class Normalizer:
    def normalize(self, x_train, x_valid):
        """
        Normalizes data according to given methods based on train statistics.
        Args:
            x_train: np.ndarray of training data
            x_valid: np.ndarray of validation data

        Returns:
            tuple of np.ndarrays which are training data and validation data
        """

        raise NotImplementedError


class ZeroToOneNormalizer(Normalizer):
    def __init__(self, min_value: Optional[Union[int, float]] = None,
                 max_value: Optional[Union[int, float]] = None,
                 axis: Optional[Union[int, float]] = None):
        self._axis = axis
        self._min_value = min_value
        self._max_value = max_value

    def normalize(self, x_train, x_valid):
        if self._min_value is not None and self._max_value is not None:
            x_train = (x_train - self._min_value) / (self._max_value - self._min_value)
            x_valid = (x_valid - self._min_value) / (self._max_value - self._min_value)
            return x_train, x_valid
        elif self._axis is not None:
            maxes = np.max(x_train, axis=self._axis)
            mins = np.min(x_train, axis=self._axis)
            x_train = (x_train - mins) / (maxes - mins)
            x_valid = (x_valid - mins) / (maxes - mins)
            return x_train, x_valid
        else:
            maxes = np.max(x_train)
            mins = np.min(x_train)
            x_train = (x_train - mins) / (maxes - mins)
            x_valid = (x_valid - mins) / (maxes - mins)
            return x_train, x_valid


class MinusOneToOneNormalizer(Normalizer):
    def __init__(self, min_value: Optional[Union[int, float]] = None,
                 max_value: Optional[Union[int, float]] = None,
                 axis: Optional[Union[int, float]] = None):
        self._axis = axis
        self._min_value = min_value
        self._max_value = max_value

    def normalize(self, x_train, x_valid):
        if self._min_value is not None and self._max_value is not None:
            x_train = (x_train - self._min_value) / (
                    self._max_value - self._min_value) * 2 - 1
            x_valid = (x_valid - self._min_value) / (
                    self._max_value - self._min_value) * 2 - 1
            return x_train, x_valid
        elif self._axis is not None:
            maxes = np.max(x_train, axis=self._axis)
            mins = np.min(x_train, axis=self._axis)
            x_train = (x_train - mins) / (maxes - mins) * 2 - 1
            x_valid = (x_valid - mins) / (maxes - mins) * 2 - 1
            return x_train, x_valid
        else:
            maxes = np.max(x_train)
            mins = np.min(x_train)
            x_train = (x_train - mins) / (maxes - mins) * 2 - 1
            x_valid = (x_valid - mins) / (maxes - mins) * 2 - 1
            return x_train, x_valid


class StandardNormalizer(Normalizer):
    def __init__(self, mean: Optional[Union[int, float]] = None,
                 std: Optional[Union[int, float]] = None,
                 axis: Optional[Union[int, float]] = None):
        self._axis = axis
        self._mean = mean
        self._std = std

    def normalize(self, x_train, x_valid):
        if self._mean is not None and self._mean is not None:
            x_train = (x_train - self._mean) / self._std
            x_valid = (x_valid - self._mean) / self._std
            return x_train, x_valid
        elif self._axis is not None:
            means = np.mean(x_train, axis=self._axis)
            stds = np.std(x_train, axis=self._axis)
            x_train = (x_train - means) / stds
            x_valid = (x_valid - means) / stds
            return x_train, x_valid
        else:
            means = np.mean(x_train)
            stds = np.std(x_train)
            x_train = (x_train - means) / stds
            x_valid = (x_valid - means) / stds
            return x_train, x_valid


class NoopNormalizer(Normalizer):
    def normalize(self, x_train, x_valid):
        return x_train, x_valid
