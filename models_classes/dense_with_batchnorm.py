from typing import *

import tensorflow as tf

from model_fn import ModelFn


class DenseWithBatchNormFn(ModelFn):
    def __init__(self, layer_sizes: List[int]):
        self.layer_sizes = layer_sizes

    def call(self, input_data, resort, is_training, classes):
        input_data = tf.layers.flatten(input_data)
        net = input_data
        for i, size in enumerate(self.layer_sizes):
            net = tf.layers.dense(net, size,
                                  kernel_initializer=tf.keras.initializers.he_uniform())
            if i < len(self.layer_sizes) - 1:
                net = tf.layers.batch_normalization(net, training=is_training)
                net = tf.nn.sigmoid(net)

        activity = tf.constant(1, dtype=tf.float32)
        tf.summary.scalar("activity", activity)
        logits = net
        y_pred = tf.nn.softmax(logits)

        y_pred_cls = tf.argmax(y_pred, axis=1)
        return logits, y_pred, y_pred_cls, {"activity": activity}
