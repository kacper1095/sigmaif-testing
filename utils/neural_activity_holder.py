import typing

import numpy as np
import tensorflow as tf


class NeuralActivityHolder(typing.NamedTuple):
    layer_index: int
    data: tf.Tensor
    shape: typing.Tuple[int, ...]

    def get_name(self):
        return f"activity_{self.layer_index}"

    def get_percentage_activity(self):
        return tf.reduce_mean(tf.reduce_sum(self.data, axis=-1) / np.prod(self.shape))


class AllNeuralActivityHolder(object):
    def __init__(self):
        self.datas: typing.List[tf.Tensor] = []
        self.shapes: typing.List[typing.Tuple[int, ...]] = []

    def get_name(self):
        return "activity"

    def add_data(self, data: tf.Tensor):
        self.datas.append(data)

    def add_shape(self, shape: typing.Tuple[int, ...]):
        self.shapes.append(shape)

    def get_percentage_activity(self):
        total_elems = sum(np.prod(shape) for shape in self.shapes)
        return tf.reduce_mean(tf.reduce_sum(
            [tf.reduce_sum(partial, axis=-1) for partial in self.datas],
            axis=0) / total_elems
                              )
