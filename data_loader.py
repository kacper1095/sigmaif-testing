import tensorflow as tf

from data_adapters.data_adapter import Adapter


def initializable_iterator_from_adapter(adapter: Adapter, is_train: bool, batch_size: int):
    gen = adapter.get_train_generator if is_train else adapter.get_valid_generator

    dataset = tf.data.Dataset.from_generator(gen,
                                             output_types=(tf.float32, tf.float32),
                                             output_shapes=(
                                                 [adapter.get_num_features()],
                                                 [adapter.get_num_classes()]
                                             ))
    if is_train:
        dataset = dataset.shuffle(buffer_size=256)

    dataset = dataset.repeat(1)
    dataset = dataset.batch(batch_size)

    iterator = dataset.make_initializable_iterator()
    return iterator


def one_shot_iterator_from_adapter(adapter: Adapter, is_train: bool, batch_size: int):
    gen = adapter.get_train_generator if is_train else adapter.get_valid_generator

    dataset = tf.data.Dataset.from_generator(gen,
                                             output_types=(tf.float32, tf.float32),
                                             output_shapes=(
                                                 [adapter.get_num_features()],
                                                 [adapter.get_num_classes()]
                                             ))
    if is_train:
        dataset = dataset.shuffle(buffer_size=256)

    dataset = dataset.repeat(1)
    dataset = dataset.batch(batch_size)

    iterator = dataset.make_one_shot_iterator()
    return iterator


def input_fn(adapter: Adapter, is_train: bool, batch_size: int):
    iterator = one_shot_iterator_from_adapter(adapter, is_train, batch_size)
    data_x, data_y = iterator.get_next()
    x = {"data": data_x}
    y = data_y

    return x, y


def train_input_fn(adapter: Adapter, batch_size: int = 32):
    def _f():
        return input_fn(adapter, is_train=True, batch_size=batch_size)

    return _f


def valid_input_fn(adapter: Adapter, batch_size: int = 32):
    def _f():
        return input_fn(adapter, is_train=False, batch_size=batch_size)

    return _f
