from typing import *

import tensorflow as tf

from model_fn import ModelFn


def clusteron_layer(inputs: tf.Tensor, units: int, radius: int,
                    layer_name: str) -> tf.Tensor:
    with tf.variable_scope(layer_name):
        input_units = inputs.get_shape().as_list()[-1]
        limit = tf.sqrt(6 / (input_units + units))
        padded_x = tf.pad(inputs, [[0, 0], [radius, radius]])
        xs = []

        for i in range(radius, input_units + radius):
            slice_length = slice(i - radius, i + radius + 1)
            xs.append(tf.reshape(padded_x[:, slice_length], (1, -1, 2 * radius + 1)))
        xs = tf.concat(xs, axis=0)

        clustering_kernels = tf.Variable(
            tf.random_uniform((input_units, 2 * radius + 1, units), -limit, limit),
            name="clustering_W",
        )

        xs = tf.matmul(xs, clustering_kernels)
        xs = tf.transpose(xs, [1, 0, 2])

        weights = tf.Variable(
            tf.random_uniform((input_units, units), -limit, limit),
            name="W"
        )
        x = tf.expand_dims(inputs, -1) * weights
        x = x * xs
        x = tf.reduce_sum(x, axis=1)
        print(x.get_shape().as_list())
    return x


class ClusteronFn(ModelFn):
    def __init__(self, layer_sizes: List[int], radius: int):
        self.layer_sizes = layer_sizes
        self.radius = radius

    def call(self, input_data, resort, is_training, classes):
        input_data = tf.layers.flatten(input_data)
        net = input_data
        neural_activities = {}
        for i, size in enumerate(self.layer_sizes):
            net = clusteron_layer(net, size, self.radius, "layer_{}".format(i))
            activity = tf.constant(1, dtype=tf.float32)
            tf.summary.scalar(f"activity_{i}", activity)
            neural_activities[f"activity_{i}"] = activity
            # if i < len(self.layer_sizes) - 1:
            #     net = tf.nn.sigmoid(net)
        logits = net
        y_pred = tf.nn.softmax(logits)

        y_pred_cls = tf.argmax(y_pred, axis=1)
        return logits, y_pred, y_pred_cls, neural_activities
