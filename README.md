# SigmaIf Testing

## Description

It's repo containing all the data and scripts used for model evaluation and research connected with this. It uses package installed using [sigmaif-tensorflow](https://gitlab.com/kacper1095/sigmaif-tensorflow) repoistory, which uses `libsigmaif_op.so` built using cloned [sigmaif-building](https://gitlab.com/kacper1095/sigmaif-building) repository.

## Requirements


- miniconda 4.5.12 - [link](https://conda.io/en/latest/miniconda.html) to download and with installation instruction  (every version above it should be fine)
- CUDA 9.0 - [link](https://developer.nvidia.com/cuda-90-download-archive?) to download. Consider using `runfile(local)` only during download and don't forget to uncheck nvidia driver overriding during installation.
- CuDNN 7.1.4 - [link](https://developer.nvidia.com/rdp/cudnn-archive) to all versions. Download version appropriate for your operating system and for CUDA 9.0.
- nvidia driver 390.77
- git lfs 2.6.1 - [link](https://github.com/git-lfs/git-lfs/wiki/Installation) to installation instruction
- cloned [`sigmaif-building`](https://gitlab.com/kacper1095/sigmaif-building/) and built package according to instruction from there
- installed `sigmaif` library using [`sigmaif-tensorflow`](https://gitlab.com/kacper1095/sigmaif-tensorflow) repository

### Your current directory structure

```
<repos_directory>
  ├── sigmaif-building       <- required
  ├── sigmaif-tensorflow     <- required
  └── sigmaif-testing        <- currently in this directory
```

## Installation
*If you already perfomed installation using `sigmaif-tensorflow` repository, you can skip this section.

### Environment
1. `conda env create -f env.yaml`
2. `source activate sigmaif`

Now you have `sigmaif` environment activated.

### Package
Be sure to have `sigmaif` environment activated using following command:
```
source activate sigmaif
```

If you want to update a package, have directory structure as mentioned above and you built package using `sigmaif-building`,
you can run `install.sh` to reassure that you have `sigmaif` library available in your environment.

### First check
Run the following command to check whether package has probably installed:
```bash
python
> from sigmaif import layer
> layer.SigmaIf
```
If no error is thrown, then your package is correctly installed.

## Usage
Be sure to have `sigmaif` environment activate using following command:
```
source activate sigmaif
```

### What can I do now?
#### Train

Run following command to train new model using GPU:
```
python training.py --model_name <full_path_to_model> \
                   --epochs <num_of_epochs_default_to_50> \
                   --batch_size <num_of_samples_per_batch_default_to_32>
```
or CPU:

```
CUDA_VISIBLE_DEVICES= python training.py --model_name <full_path_to_model> \
                   --epochs <num_of_epochs_default_to_50> \
                   --batch_size <num_of_samples_per_batch_default_to_32>
```
Trained model will be located in path pointed in `<full_path_to_model>`.


#### Check results
Anaconda's `tensorflow` installs automatically `tensorboard` as well. You can use to give look how your network performs. Following statistics are plotted:
- accuracy
- neuronal activity as percentage of all connections averaged across mini-batch
- loss

Run following command to start `tensorboard`:
```
tensorboard --logdir=<full_path_to_model>
```
where `<full_path_to_model>` is the same as one used during `training.py` call. Follow instructions displayed in the console, there is a mention that you have to enter `localhost:<port>` to see `tensorboard`. `<port>` by default is equal to `6006`.

#### Check different datasets
Currently few datasets are supported:

- Sonar (used by default)
- MNIST 
- CIFAR10
- CIFAR100
- ...

The list will grow in time. Due to small sizes of those datasets, they are held on this repo. 

To change dataset:
1. `from data_adapters import <wanted_dataset_adapter>`
2. Change construcor of already called instance of `Adapter` (ex. `SonarAdapter`) to your `<wanted_dataset_adapter>`. 

This change is general and is applicable everywhere `Adapter` instances are used (ex. in `training.py`).
#### Check different models
You can also how SigmaIf performs in comparison to fully connected network. To change model:
1. `from model_classes import <wanted_model_fn>`
2. Pass `<wanted_model_fn>` to `Model` class constructor where argument to keyword `model_fn` is passed.

This change is general and is applicable everywhere `Adapter` instances are used (ex. in `training.py`).


## Additional info
If you have `sigmaif-tensorflow` on the same directory level as this repo, you can use `install.sh` script to automatically install library (just in case new implementation of `sigmaif` came out).

## Dataset additional info
### Breast cancer
1. 16 of 699 records had nulls (as '?' character). Those were removed during evaluation.

### Credit approval
1. 37 of 690 records had nulls (as '?' character). Those were removed during evaluation.

### Votes
1. 203 of 435 records had nulls (as '?' character).Those were removed during evaluation.