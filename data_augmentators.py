from typing import *

import numpy as np


class _Augmentator(object):
    def __init__(self, seed: Optional[int] = None):
        self._seed = seed
        self.rng = np.random.RandomState(seed)

    def augment_from_generator(self, adapter_batch_generator: Generator):
        for batch_x, batch_y in adapter_batch_generator:
            batch_x = self.augment_batch(batch_x)
            yield batch_x, batch_y

            if self._seed is not None:
                new_seed = self.rng.randint(0, 10 ** 8)
                self.rng.seed(new_seed)
                self._seed = new_seed

    def augment_batch(self, batch: np.ndarray) -> np.ndarray:
        raise NotImplementedError


class Compose(_Augmentator):
    def __init__(self, augmentators: Iterable[_Augmentator], seed: Optional[int] = None):
        super().__init__(seed)
        self.augmentators = augmentators

    def augment_batch(self, batch: np.ndarray) -> np.ndarray:
        for aug in self.augmentators:
            batch = aug.augment_batch(batch)
        return batch


class MultiplicativeGaussianNoise(_Augmentator):

    def __init__(self, loc: float, scale: float, seed: Optional[int] = None):
        super().__init__(seed)
        self.loc = loc
        self.scale = scale

    def augment_batch(self, batch: np.ndarray) -> np.ndarray:
        noise = self.rng.normal(self.loc, self.scale, size=batch.shape)
        return batch + batch * noise


class AdditiveGaussianNoise(_Augmentator):

    def __init__(self, loc: float, scale: float, seed: Optional[int] = None):
        super().__init__(seed)
        self.loc = loc
        self.scale = scale

    def augment_batch(self, batch: np.ndarray) -> np.ndarray:
        noise = self.rng.normal(self.loc, self.scale, size=batch.shape)
        return batch + noise


class MultiplicativeUniformNoise(_Augmentator):

    def __init__(self, min_val: float, max_val: float, seed: Optional[int] = None):
        super().__init__(seed)
        self.min_val = min_val
        self.max_val = max_val

    def augment_batch(self, batch: np.ndarray) -> np.ndarray:
        noise = self.rng.uniform(self.min_val, self.max_val, size=batch.shape)
        return batch + batch * noise


class AdditiveUniformNoise(_Augmentator):

    def __init__(self, min_val: float, max_val: float, seed: Optional[int] = None):
        super().__init__(seed)
        self.min_val = min_val
        self.max_val = max_val

    def augment_batch(self, batch: np.ndarray) -> np.ndarray:
        noise = self.rng.uniform(self.min_val, self.max_val, size=batch.shape)
        return batch + noise


class Noop(_Augmentator):
    def augment_batch(self, batch: np.ndarray) -> np.ndarray:
        return batch
