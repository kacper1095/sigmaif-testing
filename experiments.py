import logging
import os

import tensorflow as tf

from data_adapters import *
from data_augmentators import AdditiveGaussianNoise
from descriptions import CustomScoringAugmentatorsDescription, EXPERIMENT_FOLDER, \
    StandardDescription
from models_classes import *
from normalizers import *

tf.logging.set_verbosity(tf.logging.INFO)
logger = logging.getLogger("tensorflow")
logger.setLevel(logging.INFO)

# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# create file handler which logs even debug messages
fh = logging.FileHandler('tensorflow.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)

OPTIMISER = tf.train.MomentumOptimizer(0.01, 0.8)
KFOLDS = 5
REPETITIONS = 5
BATCH_SIZE = 16
LOSS = tf.nn.softmax_cross_entropy_with_logits_v2
EPOCHS = 600
NORMALIZER = MinusOneToOneNormalizer
TABULAR_NORMALIZER = lambda: NORMALIZER(axis=0)
IMAGERY_NORMALIZER = lambda: NORMALIZER(min_value=0, max_value=255)
IMAGERY_EARLY_STOPPING_PATIENCE = 40
TABULAR_EARLY_STOPPING_PATIENCE = 100

DEFAULT_THRESHOLD = 0.6
DEFAULT_NUM_GROUPS = 7
DEFAULT_FORWARD_FADING = 0.8
DEFAULT_BACKWARD_FADING = 0.8

DEFAULT_TABULAR_NB_HIDDEN_UNITS = 32
DEFAULT_IMAGERY_NB_HIDDEN_UNITS = 128

TABULAR_DATASETS = [
    lambda: BreastCancerAdapter(),
    lambda: CreditApprovalAdapter(),
    lambda: SonarAdapter(),
    lambda: UrbanLandCover(),
    lambda: WineAdapter()
]

IMAGERY_DATASETS = [
    lambda: Cifar10Adapter(),
    lambda: MnistAdapter(),
]

IMAGERY_DATASETS_NAMES = [dataset().name for dataset in IMAGERY_DATASETS]

ALL_DATASETS = TABULAR_DATASETS + IMAGERY_DATASETS

ALL_DATASETS_NAMES = [d().name for d in ALL_DATASETS]


def populate_experiments():
    fading_factor_values = [0, 0.5, 1.0]
    for dataset in ALL_DATASETS:
        patience = IMAGERY_EARLY_STOPPING_PATIENCE if dataset.name in IMAGERY_DATASETS_NAMES else TABULAR_EARLY_STOPPING_PATIENCE
        for factor_value in fading_factor_values:
            name = "%s_ffa_%.2f" % (dataset.__class__.__name__.lower(), factor_value)
            StandardDescription(
                name=name, model_fn=FFASigmaIfBatchNormFn([128, dataset.get_num_classes()],
                                                          forward_fading=factor_value,
                                                          backward_fading=factor_value,
                                                          threshold=DEFAULT_THRESHOLD,
                                                          number_of_groups=DEFAULT_NUM_GROUPS),
                data_normalizer=NORMALIZER(axis=0), kfolds=KFOLDS, repetitions=REPETITIONS,
                batch_size=BATCH_SIZE, epochs=EPOCHS, loss_fn=LOSS,
                optimiser=OPTIMISER,
                dataset=dataset,
                patience=patience
            ).run()

            name = "%s_ffa_without_back_ffa_%.2f" % (
                dataset.__class__.__name__.lower(), factor_value)
            StandardDescription(
                name=name, model_fn=FFASigmaIfBatchNormFn([128, dataset.get_num_classes()],
                                                          forward_fading=factor_value,
                                                          backward_fading=1.0,
                                                          threshold=DEFAULT_THRESHOLD,
                                                          number_of_groups=DEFAULT_NUM_GROUPS),
                data_normalizer=NORMALIZER(axis=0), kfolds=KFOLDS, repetitions=REPETITIONS,
                batch_size=BATCH_SIZE, epochs=EPOCHS, loss_fn=LOSS,
                optimiser=OPTIMISER,
                dataset=dataset,
                patience=patience
            ).run()


def populate_thresholding_experiment():
    experiment_name = "threshold"
    thresholding_values = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7,
                           0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5,
                           1.6, 1.7, 1.8, 1.9, 2.0]

    networks = {
        "ffa": lambda hidden_neurons, adapter, thr: FFASigmaIfFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=thr,
            forward_fading=DEFAULT_FORWARD_FADING,
            backward_fading=DEFAULT_BACKWARD_FADING,
            number_of_groups=DEFAULT_NUM_GROUPS),
        "sigmaif": lambda hidden_neurons, adapter, thr: SigmaIfFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=thr,
            number_of_groups=DEFAULT_NUM_GROUPS),
    }

    for net_name, net in networks.items():
        for dataset in ALL_DATASETS:
            ds = dataset()
            current_normalizer = IMAGERY_NORMALIZER if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_NORMALIZER
            num_neurons = DEFAULT_IMAGERY_NB_HIDDEN_UNITS if ds.name in IMAGERY_DATASETS_NAMES else DEFAULT_TABULAR_NB_HIDDEN_UNITS
            patience = IMAGERY_EARLY_STOPPING_PATIENCE if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_EARLY_STOPPING_PATIENCE

            for threshold in thresholding_values:
                name = os.path.join(experiment_name, net_name, ds.name, str(threshold))
                StandardDescription(
                    name=name, model_fn=net(num_neurons, ds, threshold),
                    data_normalizer=current_normalizer(), kfolds=KFOLDS,
                    repetitions=REPETITIONS,
                    batch_size=BATCH_SIZE, epochs=EPOCHS, loss_fn=LOSS,
                    optimiser=OPTIMISER,
                    dataset=ds,
                    patience=patience
                ).run()
    for dataset in ALL_DATASETS: ds = dataset()
    num_neurons = DEFAULT_IMAGERY_NB_HIDDEN_UNITS if ds.name in IMAGERY_DATASETS_NAMES else DEFAULT_TABULAR_NB_HIDDEN_UNITS
    current_normalizer = IMAGERY_NORMALIZER if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_NORMALIZER
    patience = IMAGERY_EARLY_STOPPING_PATIENCE if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_EARLY_STOPPING_PATIENCE
    name = os.path.join(experiment_name, "dense", ds.name, "inf")
    StandardDescription(name=name, model_fn=DenseFn([num_neurons, ds.get_num_classes()]),
                        data_normalizer=current_normalizer(),
                        kfolds=KFOLDS, repetitions=REPETITIONS, batch_size=BATCH_SIZE,
                        epochs=EPOCHS, loss_fn=LOSS,
                        optimiser=OPTIMISER,
                        dataset=ds,
                        patience=patience
                        ).run()


def populate_grouping_experiment():
    experiment_name = "groupings"
    grouping_values = 1, 2, 3, 5, 7, 10, 15, 20

    networks = {
        "ffa": lambda hidden_neurons, adapter, groups: FFASigmaIfFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            forward_fading=DEFAULT_FORWARD_FADING,
            backward_fading=DEFAULT_BACKWARD_FADING,
            number_of_groups=groups),
        "sigmaif": lambda hidden_neurons, adapter, groups: SigmaIfFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            number_of_groups=groups),
    }

    for net_name, net in networks.items():
        for dataset in ALL_DATASETS:
            ds = dataset()
            current_normalizer = IMAGERY_NORMALIZER if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_NORMALIZER
            num_neurons = DEFAULT_IMAGERY_NB_HIDDEN_UNITS if ds.name in IMAGERY_DATASETS_NAMES else DEFAULT_TABULAR_NB_HIDDEN_UNITS
            patience = IMAGERY_EARLY_STOPPING_PATIENCE if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_EARLY_STOPPING_PATIENCE
            for group_value in grouping_values:
                name = os.path.join(experiment_name, net_name, ds.name, str(group_value))
                StandardDescription(
                    name=name, model_fn=net(num_neurons, ds, group_value),
                    data_normalizer=current_normalizer(), kfolds=KFOLDS,
                    repetitions=REPETITIONS,
                    batch_size=BATCH_SIZE, epochs=EPOCHS, loss_fn=LOSS,
                    optimiser=OPTIMISER,
                    dataset=ds,
                    patience=patience
                ).run()


def populate_ffa_both_sides_experiment():
    experiment_name = "ffa_both_sides"
    fadings = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]

    networks = {
        "ffa": lambda hidden_neurons, adapter, fading: FFASigmaIfFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            forward_fading=fading,
            backward_fading=fading,
            number_of_groups=DEFAULT_NUM_GROUPS),
    }

    for net_name, net in networks.items():
        for dataset in ALL_DATASETS:
            ds = dataset()
            current_normalizer = IMAGERY_NORMALIZER if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_NORMALIZER
            num_neurons = DEFAULT_IMAGERY_NB_HIDDEN_UNITS if ds.name in IMAGERY_DATASETS_NAMES else DEFAULT_TABULAR_NB_HIDDEN_UNITS
            patience = IMAGERY_EARLY_STOPPING_PATIENCE if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_EARLY_STOPPING_PATIENCE
            for fading_value in fadings:
                name = os.path.join(experiment_name, net_name, ds.name, str(fading_value))
                StandardDescription(
                    name=name, model_fn=net(num_neurons, ds, fading_value),
                    data_normalizer=current_normalizer(), kfolds=KFOLDS,
                    repetitions=REPETITIONS,
                    batch_size=BATCH_SIZE, epochs=EPOCHS, loss_fn=LOSS,
                    optimiser=OPTIMISER,
                    dataset=ds,
                    patience=patience
                ).run()


def populate_ffa_one_sideexperiment():
    experiment_name = "ffa_one_side"
    fadings = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]

    networks = {
        "ffa": lambda hidden_neurons, adapter, fading: FFASigmaIfFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            forward_fading=fading,
            backward_fading=1,
            number_of_groups=DEFAULT_NUM_GROUPS),
    }

    for net_name, net in networks.items():
        for dataset in ALL_DATASETS:
            ds = dataset()
            current_normalizer = IMAGERY_NORMALIZER if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_NORMALIZER
            num_neurons = DEFAULT_IMAGERY_NB_HIDDEN_UNITS if ds.name in IMAGERY_DATASETS_NAMES else DEFAULT_TABULAR_NB_HIDDEN_UNITS
            patience = IMAGERY_EARLY_STOPPING_PATIENCE if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_EARLY_STOPPING_PATIENCE
            for fading_value in fadings:
                name = os.path.join(experiment_name, net_name, ds.name, str(fading_value))
                StandardDescription(
                    name=name, model_fn=net(num_neurons, ds, fading_value),
                    data_normalizer=current_normalizer(), kfolds=KFOLDS,
                    repetitions=REPETITIONS,
                    batch_size=BATCH_SIZE, epochs=EPOCHS, loss_fn=LOSS,
                    optimiser=OPTIMISER,
                    dataset=ds,
                    patience=patience
                ).run()


def populate_use_convolution_experiment():
    experiment_name = "use_convolution"
    networks = {
        "ffa": lambda hidden_neurons, adapter: FFAWithConvolutionFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            forward_fading=DEFAULT_FORWARD_FADING,
            backward_fading=DEFAULT_BACKWARD_FADING,
            number_of_groups=DEFAULT_NUM_GROUPS),
        "sigmaif": lambda hidden_neurons, adapter: SigmaIfWithConvolutionFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            number_of_groups=DEFAULT_NUM_GROUPS),
        "dense": lambda hidden_neurons, adapter: DenseWithConvolutionFn(
            [hidden_neurons, adapter.get_num_classes()])
    }

    for net_name, net in networks.items():
        for dataset in IMAGERY_DATASETS:
            ds = dataset()
            current_normalizer = IMAGERY_NORMALIZER if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_NORMALIZER
            num_neurons = DEFAULT_IMAGERY_NB_HIDDEN_UNITS if ds.name in IMAGERY_DATASETS_NAMES else DEFAULT_TABULAR_NB_HIDDEN_UNITS
            patience = IMAGERY_EARLY_STOPPING_PATIENCE if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_EARLY_STOPPING_PATIENCE

            name = os.path.join(experiment_name, net_name, ds.name, "convo")
            StandardDescription(
                name=name, model_fn=net(num_neurons, ds),
                data_normalizer=current_normalizer(), kfolds=KFOLDS,
                repetitions=REPETITIONS,
                batch_size=BATCH_SIZE, epochs=EPOCHS, loss_fn=LOSS,
                optimiser=OPTIMISER,
                dataset=ds,
                patience=patience
            ).run()


def populate_additive_noise_on_validation_set_experiment():
    experiment_name = "validation_set_additive_noise"
    noise_values = [10e-5, 0.1, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0]
    networks = {
        "ffa_convo": lambda hidden_neurons, adapter: FFAWithConvolutionFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            forward_fading=DEFAULT_FORWARD_FADING,
            backward_fading=DEFAULT_BACKWARD_FADING,
            number_of_groups=DEFAULT_NUM_GROUPS),
        "sigmaif_convo": lambda hidden_neurons, adapter: SigmaIfWithConvolutionFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            number_of_groups=DEFAULT_NUM_GROUPS),
        "dense_convo": lambda hidden_neurons, adapter: DenseWithConvolutionFn(
            [hidden_neurons, adapter.get_num_classes()]),
        "ffa": lambda hidden_neurons, adapter: FFASigmaIfFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            forward_fading=DEFAULT_FORWARD_FADING,
            backward_fading=DEFAULT_BACKWARD_FADING,
            number_of_groups=DEFAULT_NUM_GROUPS),
        "sigmaif": lambda hidden_neurons, adapter: SigmaIfFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            number_of_groups=DEFAULT_NUM_GROUPS),
        "dense": lambda hidden_neurons, adapter: DenseFn(
            [hidden_neurons, adapter.get_num_classes()])
    }

    for net_name, net in networks.items():
        for dataset in IMAGERY_DATASETS:
            augmentators = {
                str(noise): AdditiveGaussianNoise(loc=0, scale=noise)
                for noise in noise_values
            }
            ds = dataset()
            current_normalizer = IMAGERY_NORMALIZER if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_NORMALIZER
            num_neurons = DEFAULT_IMAGERY_NB_HIDDEN_UNITS if ds.name in IMAGERY_DATASETS_NAMES else DEFAULT_TABULAR_NB_HIDDEN_UNITS
            patience = IMAGERY_EARLY_STOPPING_PATIENCE if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_EARLY_STOPPING_PATIENCE

            name = os.path.join(experiment_name, net_name, ds.name)
            CustomScoringAugmentatorsDescription(
                name=name, model_fn=net(num_neurons, ds),
                data_normalizer=current_normalizer(), kfolds=KFOLDS,
                repetitions=REPETITIONS,
                batch_size=BATCH_SIZE, epochs=EPOCHS, loss_fn=LOSS,
                optimiser=OPTIMISER,
                dataset=ds,
                patience=patience,
            ).run(augmentators=augmentators)


def populate_timing_experiment():
    experiment_name = "timing"
    networks = {
        "ffa": lambda hidden_neurons, adapter: FFASigmaIfFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            forward_fading=DEFAULT_FORWARD_FADING,
            backward_fading=DEFAULT_BACKWARD_FADING,
            number_of_groups=DEFAULT_NUM_GROUPS),
        "sigmaif": lambda hidden_neurons, adapter: SigmaIfFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            number_of_groups=DEFAULT_NUM_GROUPS),
        "dense": lambda hidden_neurons, adapter: DenseFn(
            [hidden_neurons, adapter.get_num_classes()])
    }

    for net_name, net in networks.items():
        for dataset in ALL_DATASETS:
            ds = dataset()
            current_normalizer = IMAGERY_NORMALIZER if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_NORMALIZER
            num_neurons = DEFAULT_IMAGERY_NB_HIDDEN_UNITS if ds.name in IMAGERY_DATASETS_NAMES else DEFAULT_TABULAR_NB_HIDDEN_UNITS
            patience = IMAGERY_EARLY_STOPPING_PATIENCE if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_EARLY_STOPPING_PATIENCE

            name = os.path.join(experiment_name, net_name, ds.name, "time")
            StandardDescription(
                name=name, model_fn=net(num_neurons, ds),
                data_normalizer=current_normalizer(), kfolds=KFOLDS,
                repetitions=REPETITIONS,
                batch_size=BATCH_SIZE, epochs=EPOCHS, loss_fn=LOSS,
                optimiser=OPTIMISER,
                dataset=ds,
                patience=patience,
            ).run()


def populate_final_experiment():
    experiment_name = "final"
    networks = {
        "ffa": {
            "breast-cancer": lambda hidden_neurons, adapter: FFASigmaIfFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=1.4,
                forward_fading=1.0,
                backward_fading=1.0,
                number_of_groups=20),
            "cifar10": lambda hidden_neurons, adapter: FFASigmaIfFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=0.5,
                forward_fading=0.1,
                backward_fading=1.0,
                number_of_groups=1),
            "crx": lambda hidden_neurons, adapter: FFASigmaIfFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=0.1,
                forward_fading=1.0,
                backward_fading=1.0,
                number_of_groups=7),
            "mnist": lambda hidden_neurons, adapter: FFAWithConvolutionFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=0.9,
                forward_fading=0.0,
                backward_fading=1.0,
                number_of_groups=1),
            "sonar": lambda hidden_neurons, adapter: FFASigmaIfFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=0.0,
                forward_fading=0.0,
                backward_fading=1.0,
                number_of_groups=1),
            "urban-land-cover": lambda hidden_neurons, adapter: FFASigmaIfFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=1.6,
                forward_fading=0.2,
                backward_fading=1.0,
                number_of_groups=2),
            "wine": lambda hidden_neurons, adapter: FFASigmaIfFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=1.9,
                forward_fading=0.0,
                backward_fading=1.0,
                number_of_groups=2)
        },
        "sigmaif": {
            "breast-cancer": lambda hidden_neurons, adapter: SigmaIfFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=1.4,
                number_of_groups=15),
            "cifar10": lambda hidden_neurons, adapter: SigmaIfWithConvolutionFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=0.6,
                number_of_groups=1),
            "crx": lambda hidden_neurons, adapter: SigmaIfFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=2.0,
                number_of_groups=10),
            "mnist": lambda hidden_neurons, adapter: SigmaIfWithConvolutionFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=1.7,
                number_of_groups=1),
            "sonar": lambda hidden_neurons, adapter: SigmaIfFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=0.1,
                number_of_groups=1),
            "urban-land-cover": lambda hidden_neurons, adapter: SigmaIfFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=1.6,
                number_of_groups=3),
            "wine": lambda hidden_neurons, adapter: SigmaIfFn(
                [hidden_neurons, adapter.get_num_classes()],
                threshold=1.6,
                number_of_groups=10),
        },
        "dense": {
            "breast-cancer": lambda hidden_neurons, adapter: DenseFn(
                [hidden_neurons, adapter.get_num_classes()]),
            "cifar10": lambda hidden_neurons, adapter: DenseFn(
                [hidden_neurons, adapter.get_num_classes()]),
            "crx": lambda hidden_neurons, adapter: DenseFn(
                [hidden_neurons, adapter.get_num_classes()]),
            "mnist": lambda hidden_neurons, adapter: DenseWithConvolutionFn(
                [hidden_neurons, adapter.get_num_classes()]),
            "sonar": lambda hidden_neurons, adapter: DenseFn(
                [hidden_neurons, adapter.get_num_classes()]),
            "urban-land-cover": lambda hidden_neurons, adapter: DenseFn(
                [hidden_neurons, adapter.get_num_classes()]),
            "wine": lambda hidden_neurons, adapter: DenseFn(
                [hidden_neurons, adapter.get_num_classes()]),
        },
    }

    for net_name, net_with_dataset in networks.items():
        for dataset in ALL_DATASETS:
            ds = dataset()
            current_normalizer = IMAGERY_NORMALIZER if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_NORMALIZER
            num_neurons = DEFAULT_IMAGERY_NB_HIDDEN_UNITS if ds.name in IMAGERY_DATASETS_NAMES else DEFAULT_TABULAR_NB_HIDDEN_UNITS
            patience = IMAGERY_EARLY_STOPPING_PATIENCE if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_EARLY_STOPPING_PATIENCE

            name = os.path.join(experiment_name, net_name, ds.name, "final")
            StandardDescription(
                name=name, model_fn=net_with_dataset[ds.name](num_neurons, ds),
                data_normalizer=current_normalizer(), kfolds=KFOLDS,
                repetitions=REPETITIONS,
                batch_size=BATCH_SIZE, epochs=EPOCHS, loss_fn=LOSS,
                optimiser=OPTIMISER,
                dataset=ds,
                patience=patience,
            ).run()


def populate_use_convolution_generic(name):
    experiment_name = f"use_convolution_{name}"
    networks_with_convo = {
        # "ffa": lambda hidden_neurons, adapter: FFAWithConvolutionFn(
        #     [hidden_neurons, adapter.get_num_classes()],
        #     threshold=DEFAULT_THRESHOLD,
        #     forward_fading=DEFAULT_FORWARD_FADING,
        #     backward_fading=DEFAULT_BACKWARD_FADING,
        #     number_of_groups=DEFAULT_NUM_GROUPS),
        "sigmaif": lambda hidden_neurons, adapter: SigmaIfWithConvolutionFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            number_of_groups=DEFAULT_NUM_GROUPS),
        # "dense": lambda hidden_neurons, adapter: DenseWithConvolutionFn(
        #     [hidden_neurons, adapter.get_num_classes()])
    }

    networks_without_convo = {
        # "ffa": lambda hidden_neurons, adapter: FFASigmaIfFn(
        #     [hidden_neurons, adapter.get_num_classes()],
        #     threshold=DEFAULT_THRESHOLD,
        #     forward_fading=DEFAULT_FORWARD_FADING,
        #     backward_fading=DEFAULT_BACKWARD_FADING,
        #     number_of_groups=DEFAULT_NUM_GROUPS),
        "sigmaif": lambda hidden_neurons, adapter: SigmaIfFn(
            [hidden_neurons, adapter.get_num_classes()],
            threshold=DEFAULT_THRESHOLD,
            number_of_groups=DEFAULT_NUM_GROUPS),
        # "dense": lambda hidden_neurons, adapter: DenseFn(
        #     [hidden_neurons, adapter.get_num_classes()])
    }

    for exp_name in ["convo", "without_convo"]:
        current_nets = networks_with_convo if exp_name == "convo" else networks_without_convo
        for net_name, net in current_nets.items():
            for dataset in IMAGERY_DATASETS:
                ds = dataset()
                current_normalizer = IMAGERY_NORMALIZER if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_NORMALIZER
                num_neurons = DEFAULT_IMAGERY_NB_HIDDEN_UNITS if ds.name in IMAGERY_DATASETS_NAMES else DEFAULT_TABULAR_NB_HIDDEN_UNITS
                patience = IMAGERY_EARLY_STOPPING_PATIENCE if ds.name in IMAGERY_DATASETS_NAMES else TABULAR_EARLY_STOPPING_PATIENCE

                name = os.path.join(experiment_name, net_name, ds.name, exp_name)
                StandardDescription(
                    name=name, model_fn=net(num_neurons, ds),
                    data_normalizer=current_normalizer(), kfolds=KFOLDS,
                    repetitions=REPETITIONS,
                    batch_size=BATCH_SIZE, epochs=EPOCHS, loss_fn=LOSS,
                    optimiser=OPTIMISER,
                    dataset=ds,
                    patience=patience
                ).run()


def main():
    os.makedirs(EXPERIMENT_FOLDER)
    populate_thresholding_experiment()
    populate_grouping_experiment()
    populate_ffa_both_sides_experiment()
    populate_ffa_one_sideexperiment()
    populate_use_convolution_experiment()
    populate_additive_noise_on_validation_set_experiment()
    populate_timing_experiment()
    populate_final_experiment()
    # populate_use_convolution_generic("reshuffle")


if __name__ == '__main__':
    main()
