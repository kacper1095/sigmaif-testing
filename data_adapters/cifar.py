import pickle as pkl

import numpy as np
from sklearn.preprocessing import LabelEncoder

from common import DATASETS_PATH, IMAGE_DATASET_USED_FRACTION
from data_adapters.data_adapter import Adapter, get_random_choice_mask_for_dataset


class Cifar10Adapter(Adapter):
    def __init__(self):
        super().__init__("cifar10")
        with open(DATASETS_PATH["cifar10"] / "label_encoder.pkl", "rb") as f:
            self.label_encoder: LabelEncoder = pkl.load(f)

        self.x_train = np.load(DATASETS_PATH["cifar10"] / "train" / "images.npy")
        self.y_train = np.load(DATASETS_PATH["cifar10"] / "train" / "labels.npy")

        self.x_valid = np.load(DATASETS_PATH["cifar10"] / "test" / "images.npy")
        self.y_valid = np.load(DATASETS_PATH["cifar10"] / "test" / "labels.npy")

        self.x_train = np.reshape(self.x_train, (-1, 32, 32, 3))
        self.x_valid = np.reshape(self.x_valid, (-1, 32, 32, 3))

        train_mask = get_random_choice_mask_for_dataset(self.x_train.shape[0],
                                                        IMAGE_DATASET_USED_FRACTION)
        valid_mask = get_random_choice_mask_for_dataset(self.x_valid.shape[0],
                                                        IMAGE_DATASET_USED_FRACTION)

        self.x_train = self.x_train.astype(np.float32)
        self.x_train = self.x_train[train_mask]

        self.y_train = self.to_one_hot(self.y_train)
        self.y_train = self.y_train[train_mask]

        self.x_valid = self.x_valid.astype(np.float32)
        self.x_valid = self.x_valid[valid_mask]

        self.y_valid = self.to_one_hot(self.y_valid)
        self.y_valid = self.y_valid[valid_mask]

    def get_num_features(self):
        return 32 * 32 * 3

    def get_num_classes(self):
        return len(self.label_encoder.classes_)

    def get_input_shape(self):
        return None, 32, 32, 3


class Cifar100Adapter(Adapter):

    def __init__(self):
        super().__init__("cifar100")
        with open(DATASETS_PATH["cifar100"] / "fine_label_encoder.pkl", "rb") as f:
            self.label_encoder: LabelEncoder = pkl.load(f)

        with open(DATASETS_PATH["cifar100"] / "coarse_label_encoder.pkl", "rb") as f:
            self.coarse_label_encoder: LabelEncoder = pkl.load(f)

        self.x_train = np.load(DATASETS_PATH["cifar100"] / "train" / "images.npy")
        self.y_train = np.load(DATASETS_PATH["cifar100"] / "train" / "labels.npy")

        self.x_valid = np.load(DATASETS_PATH["cifar100"] / "test" / "images.npy")
        self.y_valid = np.load(DATASETS_PATH["cifar100"] / "test" / "labels.npy")

        train_mask = get_random_choice_mask_for_dataset(self.x_train.shape[0],
                                                        IMAGE_DATASET_USED_FRACTION)
        valid_mask = get_random_choice_mask_for_dataset(self.x_valid.shape[0],
                                                        IMAGE_DATASET_USED_FRACTION)

        self.x_train = self.x_train.astype(np.float32)
        self.x_train = self.x_train[train_mask]

        self.y_train = self.to_one_hot(self.y_train)
        self.y_train = self.y_train[train_mask]

        self.x_valid = self.x_valid.astype(np.float32)
        self.x_valid = self.x_valid[valid_mask]

        self.y_valid = self.to_one_hot(self.y_valid)
        self.y_valid = self.y_valid[valid_mask]

    def get_num_features(self):
        return 32 * 32 * 3

    def get_num_classes(self):
        return len(self.label_encoder.classes_)

    def get_input_shape(self):
        return None, 32, 32, 3
