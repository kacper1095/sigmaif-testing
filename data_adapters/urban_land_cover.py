import zipfile

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder

from common import DATASETS_PATH
from data_adapters.data_adapter import Adapter


class UrbanLandCover(Adapter):
    def __init__(self):
        super().__init__("urban-land-cover")

        with zipfile.ZipFile(
                DATASETS_PATH["urban-land-cover"] / "Urban land cover.zip") as zf:
            with zf.open("training.csv") as csv_file:
                training_data = pd.read_csv(csv_file)
            with zf.open("testing.csv") as csv_file:
                testing_data = pd.read_csv(csv_file)
        self._raw_data = training_data.append(testing_data)
        self._column_names = self._raw_data.columns
        self._class_column = self._column_names[0]
        self._features_column = self._column_names[1:]

        self._x = self._raw_data[self._features_column].values.astype(np.float32)
        self._y = self._raw_data[self._class_column].values

        self._le = LabelEncoder()
        self._le.fit(self._y)
        self._y = self._le.transform(self._y)
        self._y = self.to_one_hot(self._y)

        self._rng = np.random.RandomState(0)
        permutation = self._rng.permutation(range(len(self._x)))
        self._x, self._y = self._x[permutation], self._y[permutation]

        split_index = int(0.8 * len(self._x))

        self.x_train, self.y_train = self._x[:split_index], self._y[:split_index]
        self.x_valid, self.y_valid = self._x[split_index:], self._y[split_index:]

    def get_num_features(self):
        return len(self._features_column)

    def get_num_classes(self):
        return len(self._le.classes_)

    def get_input_shape(self):
        return None, self.get_num_features()
