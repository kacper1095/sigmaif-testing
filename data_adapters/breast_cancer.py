import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder

from common import DATASETS_PATH
from data_adapters.data_adapter import Adapter


class BreastCancerAdapter(Adapter):

    def __init__(self):
        super().__init__("breast-cancer")
        self._column_names = ["id"] + list(range(9)) + ["class"]

        self._features_column = self._column_names[1:-1]
        self._class_column = self._column_names[-1]
        self._raw_data = pd.read_csv(
            DATASETS_PATH['breast-cancer'] / 'breast-cancer-wisconsin.data', header=None,
            names=self._column_names)

        self._raw_data.drop(["id"], axis=1, inplace=True)
        self._raw_data["class"].replace({2: "benign", 4: "malignant"}, inplace=True)
        self._raw_data = self._raw_data[self._raw_data[5] != "?"]

        self._x = self._raw_data[self._features_column].values
        self._y = self._raw_data[self._class_column].values
        self._x = self._x.astype(np.float32)

        self._le = LabelEncoder()
        self._le.fit(self._y)
        self._y = self._le.transform(self._y)
        self._y = self.to_one_hot(self._y)

        self._rng = np.random.RandomState(0)
        permutation = self._rng.permutation(range(len(self._x)))
        self._x, self._y = self._x[permutation], self._y[permutation]

        split_index = int(0.8 * len(self._x))

        self.x_train, self.y_train = self._x[:split_index], self._y[:split_index]
        self.x_valid, self.y_valid = self._x[split_index:], self._y[split_index:]

    def get_num_features(self):
        return len(self._features_column)

    def get_num_classes(self):
        return len(self._le.classes_)

    def get_input_shape(self):
        return None, self.get_num_features()
