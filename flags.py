import tensorflow as tf

import common


def get_flags():
    tf.flags.DEFINE_string("model_output",
                           "trained_models/dummy/" + common.get_timestamp(),
                           "Model name to save")
    tf.flags.DEFINE_integer("epochs", 50, "Number of epochs")
    tf.flags.DEFINE_integer("batch_size", 2, "Batch size")
    tf.flags.DEFINE_integer("kfolds", 10, "Number of folds for cross validation")
    args = tf.flags.FLAGS
    return args
